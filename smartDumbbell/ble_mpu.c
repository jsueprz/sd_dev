#include "sdk_common.h"
#include "ble_srv_common.h"
#include "nordic_common.h"
#include "app_util.h"
#include "ble_mpu.h"
#include <string.h>
#include "nrf_gpio.h"
#include "boards.h"
#include "nrf_log.h"

/**@brief Function for handling the Connect event.
 *  sets and stores the connection handle
 * @param[in]   p_mpu            LED Button Service structure.
 * @param[in]   p_ble_evt        Event received from the BLE stack.
 */
static void on_connect(ble_mpu_t * p_mpu, ble_evt_t const * p_ble_evt)
{
    p_mpu->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief Function for handling the Disconnect event.
 *  resets the connection handle associated with the service
 * @param[in]   p_mpu   LED Button Service structure.
 * @param[in]   p_ble_evt        Event received from the BLE stack.
 */
static void on_disconnect(ble_mpu_t * p_mpu, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_mpu->conn_handle = BLE_CONN_HANDLE_INVALID;
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_mpu            LED Button Service structure.
 * @param[in]   p_ble_evt        Event received from the BLE stack.
 */
static void on_write(ble_mpu_t * p_mpu, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = (ble_gatts_evt_write_t *) &p_ble_evt->evt.gatts_evt.params.write;
    
    if ((p_evt_write->handle == p_mpu->sensor_value_handles.value_handle) && (p_evt_write->len == 1) && (p_mpu->evt_handler != NULL))
    {
      NRF_LOG_INFO("Write on sensor value char = %d\r\n", p_evt_write->data[0]);
    }
 
    // Check if the Custom value CCCD is written to and that the value is the appropriate length, i.e 2 bytes.
    if ((p_evt_write->handle == p_mpu->sensor_value_handles.cccd_handle) && (p_evt_write->len == 2)
       )
    {
        // CCCD written, call application event handler
        if (p_mpu->evt_handler != NULL)
        {
            ble_mpu_evt_t evt;
 
            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_SENSOR_VALUE_EVT_NOTIFICATION_ENABLED;
            }
            else
            {
                evt.evt_type = BLE_SENSOR_VALUE_EVT_NOTIFICATION_DISABLED;
            }
 
            p_mpu->evt_handler(p_mpu, &evt);
        }
    }
}

void ble_mpu_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_mpu_t * p_mpu = (ble_mpu_t *) p_context;
 
    NRF_LOG_INFO("BLE event received. Event type = %d\r\n", p_ble_evt->header.evt_id); 
 
    if (p_mpu == NULL || p_ble_evt == NULL)
    {
        return;
    }
 
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_mpu, p_ble_evt);
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_mpu, p_ble_evt);
            break;
            
        case BLE_GATTS_EVT_WRITE:
            on_write(p_mpu, p_ble_evt);
            break;
            
        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for adding the Sensor value characteristic as a part of the MPU service.
 *
 */
static uint32_t sensor_value_char_add(ble_mpu_t * p_mpu)
{
    uint32_t   err_code;
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read          = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.char_props.notify        = 1;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_cccd_md                = &cccd_md;
    char_md.p_sccd_md                = NULL;

    // Add the MPU Data I/O Characteristic UUID
    ble_uuid128_t base_uuid = {BLE_UUID_SENSOR_VALUE_CHAR_BASE_UUID};
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_mpu->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    
    ble_uuid.type = p_mpu->uuid_type;
    ble_uuid.uuid = BLE_UUID_SENSOR_VALUE_CHAR_UUID;

    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    // Configure the characteristic value
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = sizeof(uint8_t);
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = sizeof(uint8_t);
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_mpu->service_handle, &char_md, &attr_char_value, &p_mpu->sensor_value_handles);

}

/**@brief Function for adding the Control Point characteristic as a part of the MPU service.
 *
 */
static uint32_t control_point_char_add(ble_mpu_t * p_mpu)
{
    uint32_t   err_code;
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read          = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.char_props.notify        = 0;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_sccd_md                = NULL;

    // Add the MPU Data I/O Characteristic UUID
    ble_uuid128_t base_uuid = {BLE_UUID_CONTROL_POINT_CHAR_BASE_UUID};
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_mpu->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    
    ble_uuid.type = p_mpu->uuid_type;
    ble_uuid.uuid = BLE_UUID_CONTROL_POINT_UUID;

    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    // Configure the characteristic value
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = sizeof(uint8_t);
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = sizeof(uint8_t);
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_mpu->service_handle, &char_md, &attr_char_value, &p_mpu->control_point_handles);

}
 
uint32_t ble_mpu_init(ble_mpu_t * p_mpu, const ble_mpu_init_t * p_mpu_init)
{
    if (p_mpu == NULL || p_mpu_init == NULL)
    {
        return NRF_ERROR_NULL;
    }

    uint32_t   err_code;
    ble_uuid_t ble_uuid;

    // Initialize service structure
    p_mpu->conn_handle               = BLE_CONN_HANDLE_INVALID;
    p_mpu->evt_handler = p_mpu_init->evt_handler;

    // Add mpu Service UUID
    ble_uuid128_t base_uuid = {BLE_UUID_MPU_SERVICE_BASE_UUID};
    err_code =  sd_ble_uuid_vs_add(&base_uuid, &p_mpu->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_mpu->uuid_type;
    ble_uuid.uuid = BLE_UUID_MPU_SERVICE_UUID;

    // Add the Custom Service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_mpu->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = sensor_value_char_add(p_mpu);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = control_point_char_add(p_mpu);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }


    return NRF_SUCCESS;
}

uint32_t ble_mpu_sensor_value_update(ble_mpu_t * p_mpu, uint8_t sensor_value)
{

    if (p_mpu == NULL)
    {
        return NRF_ERROR_NULL;
    }

    uint32_t err_code = NRF_SUCCESS;
    ble_gatts_value_t gatts_value;
 
    // Initialize value struct.
    memset(&gatts_value, 0, sizeof(gatts_value));
 
    gatts_value.len     = sizeof(uint8_t);
    gatts_value.offset  = 0;
    gatts_value.p_value = &sensor_value;
    
    // Update database.
    err_code = sd_ble_gatts_value_set(p_mpu->conn_handle,
                                      p_mpu->sensor_value_handles.value_handle,
                                      &gatts_value);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Send value if connected and notifying.
    if ((p_mpu->conn_handle != BLE_CONN_HANDLE_INVALID)) 
    {
        ble_gatts_hvx_params_t hvx_params;
 
        memset(&hvx_params, 0, sizeof(hvx_params));
 
        hvx_params.handle = p_mpu->sensor_value_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset = gatts_value.offset;
        hvx_params.p_len  = &gatts_value.len;
        hvx_params.p_data = gatts_value.p_value;
 
        err_code = sd_ble_gatts_hvx(p_mpu->conn_handle, &hvx_params);
        NRF_LOG_INFO("sd_ble_gatts_hvx result: %x. \r\n", err_code); 
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
        NRF_LOG_INFO("sd_ble_gatts_hvx result: NRF_ERROR_INVALID_STATE. \r\n"); 
    }
 
    return err_code;
}
