#ifndef BLE_MPU_H
#define BLE_MPU_H
 
#include <string.h>
#include "ble.h"
#include "ble_srv_common.h"

#ifndef BLE_MPU_BLE_OBSERVER_PRIO
#define BLE_MPU_BLE_OBSERVER_PRIO 2
#endif
 
/* Macro used for instantiating and defining the service object*/
#define BLE_MPU_DEF(_name)                                                                 \
static ble_mpu_t _name;                                                                    \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                        \
                     BLE_MPU_BLE_OBSERVER_PRIO,                                            \
                     ble_mpu_on_ble_evt, &_name)

/**
*   MPU Service UUID: 8F07FC4F-27B9-4A57-953F-075D75216009
*   Sensor Value Characteristic UUID: CF946A71-DE2F-46E8-BE47-C28F635145B6
*   Device type Characteristic  UUID: D7DA320B-E9B7-4924-852B-BBC296099FE9
*   Sensor Recolution Characteristic UUID: 6A07B0A9-74D8-4AD3-993B-43F0A3ED1613
*   Control point Characteristic UUID: FFEF0E24-A367-46E5-AF95-EF4FB9EDA4E6
*  The bytes need to be in reverse order to match the UUID from the spec 
*  Note: They are stored in little endian, meaning that the Least Significant Byte is stored first. 
*/
#define BLE_UUID_MPU_SERVICE_BASE_UUID                {0x09, 0x60, 0x21, 0x75, 0x5D, 0x07, 0x3F, 0x95, 0x57, 0x4A, 0xB9, 0x27, 0x4F, 0xFC, 0x07, 0x8F}
#define BLE_UUID_SENSOR_VALUE_CHAR_BASE_UUID          {0xB6, 0x45, 0x51, 0x63, 0x8F, 0xC2, 0x47, 0xBE, 0xE8, 0x46, 0x2F, 0xDE, 0x71, 0x6A, 0x94, 0xCF}
//#define BLE_UUID_DEVICE_TYPE_CHAR_BASE_UUID           {0xE9, 0x9F, 0x09, 0x96, 0xC2, 0xBB, 0x2B, 0x85, 0x24, 0x49, 0xB7, 0xE9, 0x0B, 0x32, 0xDA, 0xD7}
//#define BLE_UUID_SENSOR_RESOLUTION_CHAR_BASE_UUID     {0x13, 0x16, 0xED, 0xA3, 0xF0, 0x43, 0x3B, 0x99, 0xD3, 0x4A, 0xD8, 0x74, 0xA9, 0xB0, 0x07, 0x6A}
#define BLE_UUID_CONTROL_POINT_CHAR_BASE_UUID         {0xE6, 0xA4, 0xED, 0xB9, 0x4F, 0xEF, 0x95, 0xAF, 0xE5, 0x46, 0x67, 0xA3, 0x24, 0x0E, 0xEF, 0xFF}

#define BLE_UUID_MPU_SERVICE_UUID                     0xBF0A
#define BLE_UUID_SENSOR_VALUE_CHAR_UUID               0xBF0B
//#define BLE_UUID_DEVICE_TYPE_CHAR_UUID                0xBF0C
//#define BLE_UUID_SENSOR_RESOLUTION_UUID               0xBF0D
#define BLE_UUID_CONTROL_POINT_UUID                   0xBF0C

/* This is used by the control point char to define the type of device*/
#define BLE_DEVICE_TYPE_DUMBBELL              0
#define BLE_DEVICE_TYPE_BAR                   1

/* This is used by the control point char to mode of MPU operates*/
#define MPU_MODE_NORMAL_MODE_16B              0x00
#define MPU_MODE_NORMAL_MODE_8B               0x02
#define MPU_PROCESSED_DATA_16B                0x18
#define MPU_PROCESSED_DATA_8B                 0x12
#define MPU_POWERSAVING_QUATERNION            0x4A
#define MPU_ACC_GYR                           0x80

/** Define the different MPU event types and a data structure to represent a MPU event.
  * MPU Service event type. */
typedef enum
{
    BLE_SENSOR_VALUE_EVT_NOTIFICATION_ENABLED,                             /**&lt; Sensor value notification enabled event. */
    BLE_SENSOR_VALUE_EVT_NOTIFICATION_DISABLED,                            /**&lt; Sensor value notification disabled event. */
} ble_mpu_evt_type_t;

/**@brief MPU Service event. */
typedef struct{
  ble_mpu_evt_type_t evt_type;
} ble_mpu_evt_t;

/** Forward-declare the MPU service data structure
 *  Forward declaration of the ble_mpu__t type. */
typedef struct ble_mpu_s ble_mpu_t;

/** Define the MIDI Data I/O write handler functio */
typedef void (*ble_mpu_evt_handler_t) (ble_mpu_t * p_mpu, ble_mpu_evt_t * p_evt);

/**@brief MPU Service init structure. This contains all options and data needed for
 *        initialization of the service.U sed to configure a callback function that can be called back to the main application*/
typedef struct
{
    ble_mpu_evt_handler_t  evt_handler;                    /**&lt; Event handler to be called when a MPU event occurs. */
    uint8_t                initial_resolution_value;       /**< Initial resolution value */
    uint8_t *              p_device_type;                  /**< Value fixed according the device */
    uint8_t *              p_mpu_mode;
    security_req_t         bl_rd_sec;                      /**< Security requirement for reading the BL characteristic value. */
    security_req_t         bl_cccd_wr_sec;                 /**< Security requirement for writing the BL characteristic CCCD. */
    security_req_t         bl_report_rd_sec;               /**< Security requirement for reading the BL characteristic descriptor. */

} ble_mpu_init_t;

/**@brief MPU Service structure. This contains service handle, any characteristic handles,
 * the UUID type, connection handle, and more status info for the service. */
struct ble_mpu_s
{
    ble_mpu_evt_handler_t         evt_handler;   
    uint16_t                      service_handle;                 /**< Handle of Custom Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t      sensor_value_handles;           /**< Handles related to the Sensor Value characteristic. */
    //ble_gatts_char_handles_t      device_type_handles;            /**< Handles related to the Device type characteristic. */
    //ble_gatts_char_handles_t      sensor_resolution_handles;      /**< Handles related to the Sensor resolution characteristic. */
    ble_gatts_char_handles_t      control_point_handles;          /**< Handles related to the Control point characteristic. */
    uint16_t                      conn_handle;                    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    uint8_t                       uuid_type; 
};

/**@brief Function for initializing the MPU Service.
 *  This function gets called from the main application to initialize the service
 * @param[out]  p_mpu       MPU Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_mpu_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_mpu_init(ble_mpu_t * p_mpu, const ble_mpu_init_t * p_mpu_init);

/**@brief Function for handling the Application's BLE Stack events.
 *  This will get called by the stack whenever a BLE event is reported and may need to be processed by the MPU service
 * @details Handles all events from the BLE stack of interest to the MPU Service.
 *
 *
 * @param[in]   p_mpu      LED Button Service structure.
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 */
void ble_mpu_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

#endif /* BLE_MPU_H */